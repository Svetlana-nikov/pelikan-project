$(document).ready(function() {

    $(function() {
        $('.item').matchHeight();
        $('.matchHeight').matchHeight();
        $('.one-box').matchHeight();
        $('.one-product h5').matchHeight();
        $('.one-product p').matchHeight();
    });

    //Login Form
    $(".login-form").hover(
        function() {
            $(this).addClass("hover");
        },
        function() {
            $(this).removeClass("hover");
        }
    );

    $('.one-step .btn').click(function(){
        $(this).toggleClass('active');
    });

    $("body").on("click", "a.dropdown-toggle", function() {
        setTimeout(function() {

            $('.menu-height').matchHeight();
            $('.list-box').matchHeight();

        }, 1000);

        $(window).resize(function() {
            $('.menu-height').matchHeight();
            $('.list-box').matchHeight();

        });
    });

    //Remove placeholder on click
    $("input,textarea").each(function() {
        $(this).data('holder', $(this).attr('placeholder'));

        $(this).focusin(function() {
            $(this).attr('placeholder', '');
        });

        $(this).focusout(function() {
            $(this).attr('placeholder', $(this).data('holder'));
        });
    });


    // CUSTOM DROP-DOWN SETTINGS
    $('.dropdown').on('show.bs.dropdown', function () {

        $(this).siblings('.open').removeClass('open').find('a.dropdown-toggle').attr('data-toggle', 'dropdown');
        $(this).find('a').removeAttr('data-toggle');

        $('html').on('click', function() {
            if ($('.navbar-nav li.dropdown').is('.open')) {
                $('.navbar-nav li.dropdown > a').parent().removeClass('open');
                $('.navbar-nav li.dropdown > a').attr('data-toggle', 'dropdown');
            }
        });

    });


// REDIRECT TO PAGE AFTER SECOND CLICK
    $('#main-menu a.dropdown-toggle').click(function() {
        if($(this).parent('.dropdown.open').size() > 0) {
            window.location.href = $(this).attr('href');
        }
    });



    $('.top-search button').click(function(){
        $('.top-search').toggleClass('active');
    });

    var param = $('.top-bar .nav').clone();
    var fisrtmenu = $('.top-bar .nav').addClass('first');

        if ( $(window).width() < 768) {

            $('.top-bar .nav').insertBefore('#main-menu ul.nav');
        }

    window.onresize = function() {

        if ( $(window).width() < 768) {
            $('.top-bar .nav').addClass('first');
            $('.top-bar .nav').insertBefore('#main-menu ul.nav');
        }


        if ( $(window).width() > 768) {
            $('#main-menu ul.nav').remove('ul.first');
            fisrtmenu.remove();
            $(param).appendTo('.header-small-menu .top-bar');
        }
    };

});

//Custom Select
$("select").simpleselect();

jQuery('form').on('submit', function(e){
    e.preventDefault();
});